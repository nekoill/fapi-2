from fastapi import FastAPI
from services.backend.app.app import usage
import os.path

app = FastAPI()


storage_file = os.path.join(
    os.path.abspath(__file__).replace(
        "main.py", "services/backend/db/storage.data"))


def main():
    print('\n||=======================================================================================================================================================================||')  # noqa: E501
    print(f'||\t\t\t\t\t\t\t{storage_file}\t\t\t\t\t\t\t ||\n||')
    usage()
    print('||\n||==========================================================================================================================================================================\n')  # noqa: E501
    app.openapi_schema(
        title="FaAPI",
        description="FaAPI",
        version="0.1.0",
    )
    return FastAPI


if __name__ == "__main__":
    main()
