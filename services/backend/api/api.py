from fastapi import FastAPI
import json
import os.path
import uvicorn

app = FastAPI()
storage_file = os.path.join(
    os.path.abspath(__file__).replace(
        "main.py", "db/storage.data"))


def usage() -> str:
    m1 = '\n\t\t\t\t\t\t\t\t\t\tHello friend!'
    m2 = '\n\t\t\t\t\t\t\t\t\t\tMy name is FAPI'
    m3 = '\n\t\t\t\t\t\t\t\t\t\tA FastAPI for KeVaSto.Py\n\n'
    print(m1, m2, m3)


@app.get("/")
def index() -> str:
    usage()
    with open(storage_file) as o:
        jlo = json.load(o)
        print(jlo.items())
    return {'response': 200, 'message': jlo}


@app.get("/data/get/{key}")
async def get_by_id(key: int) -> dict[int, any]:
    if not key:
        return None
    else:
        with open(storage_file) as o:
            jlo = json.load(o)
            print(jlo.get(key))
            return jlo.get(key)


"""
# Will add value to the last item in storage file
@app.post("/data/post/{val}")
async def add_by_id(val: str) -> dict[int, str] | None:
    if not val:
        return None
    else:
        with open(storage_file) as o:
            jlo = json.load(o)
            jlo[len(jlo) + 1] = val
            with open(storage_file, 'w') as o:
                json.dump(jlo, o, indent=4)
            return jlo.get(len(jlo))


@app.post("/data/post/{key}={val}")
async def post_item(key: int, val: any) -> dict[int, any]:
    with open(storage_file) as o:
        jlo = json.load(o)
        if jlo.get(key):
            put_item(key, val)
        else:
            jlo.update({key: val})
            with open(storage_file, 'w') as o:
                json.dump(jlo, o)
        return jlo


@app.put("/data/put/{key}={val}")
async def put_item(key: int, val: any) -> dict[int, any]:
    with open(storage_file) as o:
        jlo = json.load(o)
        if jlo.get(key):
            with open(storage_file, "w") as o:
                if type(jlo.get(key)) == type(val):
                    jlo_ = jlo.get(key)
                    jlo[key].update({key: [jlo_, val]})
                    json.dump(jlo, o, indent=4)
                else:
                    jlo_ = jlo.get(key)
                    tuple(jlo[key]).append(jlo_, val)
                return jlo.get(key)
        else:
            return None


@app.delete("/data/delete/{key}")
async def delete_item(key: int) -> dict[int, any]:
    with open(storage_file) as o:
        jlo = json.load(o)
        if jlo.get(key):
            del jlo[key]
"""


if __name__ == "__main__":
    uvicorn.run(app, host="127.0.0.1", port=8080)
