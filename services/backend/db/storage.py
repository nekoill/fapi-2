import os.path
import json

storage_file = os.path.join(os.path.dirname(__file__), 'storage.data')

TEST_DATA: dict[str, int] = {
    'item_id': 1,
    'item_name': 'item #1',
    'item_category': 'category #1',
    'item_subcategories': [
        'subcategory #1',
        'subcategory #2',
        'subcategory #3'
    ],
    'item_availability': {
        'Moscow North-East, Altuf\'evo': 14,
        'Moscow North, Rechnoy Vokzal': 69,
        'Moscow West, Strogino': 42
    },
    'item_price_no_tax': 150.05
}


def init_storage():
    if not storage_file:
        with open(storage_file, "w") as o:
            cached_store: dict[str, any] = {}
            cached_store.update(TEST_DATA)
            json.dump(cached_store, o, indent=4)
            print(f'Storage file created at: {storage_file}')
            return json.load(o)


if __name__ == "__main__":
    init_storage()
