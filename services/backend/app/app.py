from fastapi import FastAPI

app = FastAPI()


def usage() -> str:
    m0 = '||\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t ||\n'
    m1 = '||\t\t\t\t\t\t\t\t\t\tHello friend!\t\t\t\t\t\t\t\t\t\t\t||'
    m2 = '||\t\t\t\t\t\t\t\t\t\tMy name is FAPI'
    m3 = '\n||\t\t\t\t\t\t\t\t\t\tA FastAPI for KeVaSto.Py'
    print(m0, m1, m2, m3)


def Err(errcode: str) -> str:
    if errcode.startswith(4):
        ret1 = '\n\t\tI\'m sorry Dave'
        ret2 = '\n\tI\'m afraid I can\'t do that'
        print(ret1, ret2)
    else:
        usage()


if __name__ == "__main__":
    usage()
