import Enum


class Error(Enum):
    err_category: dict[int, str]
    errcode: int
    err_msg: str

    def __init__(self, errcode: int, err_msg: str):
        self.errcode = errcode
        self.err_msg = err_msg

    def __str__(self):
        return f'{self.err_category[self.errcode]}:{self.err_subcategory[self.errcode]}:{self.err_msg}'  # noqa: E501


class ErrCodeMap(Error):
    err_category: dict[int, str] = {
        1: 'Arg_Err',
        2: 'DB_Err',
        3: 'File_Err',
        4: 'Req_Err',
        5: 'Auth_Err',
    }
    err_subcategory: dict[int, str] = {
        0: 'General_Err',
        1: 'Spelling_Err',
        2: 'Wrong_Arg',
        3: 'Wrong_Number_of_Args',
        4: 'Wrong_Type_of_Arg',
        5: 'Wrong_Value_of_Arg',
        6: 'Wrong_Type_of_Value'
    }
